package org.grails.guides

class BootStrap {

    def init = { servletContext ->

        def nissan = new Make(name: 'Nissan').save()
        def ford = new Make(name: 'Ford').save()

        def titan = new Model(name: 'Titan', make: nissan).save()
        def leaf = new Model(name: 'Leaf', make: nissan).save()
        def windstar = new Model(name: 'Windstar', make: ford).save()

        new Vehicle(name: 'Pickup',  make: nissan, model: titan, year: 2012).save()
        new Vehicle(name: 'Economy', make: nissan, model: leaf, year: 2014).save()
        new Vehicle(name: 'Minivan', make: ford, model: windstar, year: 1990).save()

        User user = new User(username: 'admin', password: 'admin').save()
        Role role = new Role(authority: 'ROLE_ADMIN').save()
        UserRole.create user, role

        user = new User(username: 'user', password: 'user').save()
        role = new Role(authority: 'ROLE_USER').save()
        UserRole.create user, role
    }
    def destroy = {
    }
}
